import React from 'react';
import ReactDOM from 'react-dom';
import UserEntry from './UserEntry.js'

class UserList extends React.Component {
  constructor(props) {
    super()
    this.db = props.db;
  }

  componentDidMount() {
    this.fetchUserData();
    this.fetchAccountData();
  }

  fetchUserData(){
    this.db.ref('users/').once('value').then((snapshot) => {
      this.setState({users: snapshot.val()})
    });
    console.log("Users Loaded");
  }

  fetchAccountData(){
    this.db.ref('accounts/').once('value').then((snapshot) => {
      this.setState({accounts: snapshot.val()})
    });
    console.log("Accounts Loaded");
  }

  render() {
    console.log("New State");
    if (this.state && this.state.users) {
      let userEntries = []
      for (let userKey of Object.keys(this.state.users)) {
        console.log(this.state.accounts !== undefined ? this.state.accounts[this.state.users[userKey].account] : null);
        userEntries.push(<UserEntry
          db={this.db}
          name={this.state.users[userKey].name}
          key={userKey}
          userId={userKey}
          accountId={this.state.users[userKey].account}
          account={this.state.accounts !== undefined ? this.state.accounts[this.state.users[userKey].account] : null}
        />)
      }
      console.log(this.state.accounts === undefined);
      return userEntries
    } else {
      return <div className="userentry"><h1 className="name">Loading Users...</h1></div>
    }
  }
}

export default UserList;
