import React from 'react';
import ReactDOM from 'react-dom';
import StarRating from './StarRating.js'

class UserEntry extends React.Component {
  constructor(props) {
    super()
    this.db = props.db;
    this.userId = props.userId;
    this.accountId = props.accountId;
    this.name = props.name
    this.account = props.account;
  }

  componentWillReceiveProps(props) {
    this.db = props.db;
    this.userId = props.userId;
    this.accountId = props.accountId;
    this.name = props.name
    this.account = props.account;
  }

  render() {
    let apps = []
    // console.log(this.account);
    if (this.account !== null) {
      for (let appKey of Object.keys(this.account.apps)) {
        apps.push(<div class="app" key={appKey}>
        <span>{this.account.apps[appKey].title}</span>
        <StarRating
          maxstars={5}
          stars={this.account.apps[appKey].stars ? this.account.apps[appKey].stars : 0}
          updateCallback={(stars)=>{
            this.db.ref(`accounts/${this.accountId}/apps/${appKey}`).update({
              stars: stars
            });
          }}
        />
        </div>)
      }
    } else {
      apps.push(<div class="app" >Loading Account...</div>)
    }
    return (
      <div className="userentry">
        <div className="name">{this.name}</div>
        <div className="accountid">{this.accountId}</div>
        {apps}
      </div>
    )
  }

}

export default UserEntry;
