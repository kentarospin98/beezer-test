import React from 'react';
import ReactDOM from 'react-dom';

class StarRating extends React.Component {
  constructor(props) {
    super();
    this.maxstars = props.maxstars;
    this.updateCallback = props.updateCallback;
    this.state = {
      currentstars: props.stars
    }
  }

  updateStars = (stars)=> {
    this.setState({currentstars: stars});
    this.updateCallback(stars);
  }

  render() {
    let stars = [];
    for (var i = 0; i < this.maxstars; i++) {
      stars.push(<span onClick={(e)=>{this.updateStars(e.target.attributes.star.nodeValue)}} star={i+1} key={i}>{i < this.state.currentstars ? "❤️" : "🤍"}</span>);
    }
    return <span className="starrating">{stars}</span>;
  }
}

export default StarRating;
