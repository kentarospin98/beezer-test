import './App.css';
import UserList from './components/UserList.js'
import React from 'react';
import ReactDOM from 'react-dom';


class App extends React.Component {
  constructor(props) {
    super()
    this.db = props.db;
  }

  render() {
    return (
      <div className="App">
        <div className="header">
          <h1 id="title">Beezer Coding Challenge</h1>
        </div>
        <UserList db={this.db}/>
      </div>
    );
  }
}

export default App;
