import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import firebase from 'firebase';

var config =  {
  apiKey: "AIzaSyAVhJutzBMAooerC6ekS9tyZ-VQgWnJdu4",
  authDomain: "beezer-test-833aa.firebaseapp.com",
  databaseURL: "https://beezer-test-833aa-default-rtdb.firebaseio.com",
  projectId: "beezer-test-833aa",
  storageBucket: "beezer-test-833aa.appspot.com",
  messagingSenderId: "544395905358",
  appId: "1:544395905358:web:b8cc1682f2077c5426f61f",
  measurementId: "G-F3WTM8FK6Z"
};


firebase.initializeApp(config);

var firedb = firebase.database();

ReactDOM.render(
  <React.StrictMode>
    <App db={firedb}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
